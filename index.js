const express = require('express')
const app = express()
const port = 3000
const mongoose = require('mongoose');
const mongoConfig = { useNewUrlParser: true }
const bodyParser = require('body-parser')

//Base de datos
mongoose.connect('mongodb://localhost:27017/test', mongoConfig);

//Configuración de express
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())


const rutas = require('./rutas')
app.use('/', rutas)


app.listen(port, () => console.log(`Example app listen on port ${port}!`))

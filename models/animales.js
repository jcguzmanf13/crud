const mongoose = require('mongoose')

var schema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
    },
    numeroPatas: {
        type: Number,
        required: true,
    },
    extinguido: {
        type: Boolean,
        required: true,
    },
})

var model = mongoose.model('Animal', schema)

module.exports = model

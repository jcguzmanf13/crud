const router = require('express').Router()
const Animales = require('./models/animales')


router.get('/animales/', (req, res) => {
    Animales.find({})
        .then((data) => {
            console.log(data);
            res.json(data)
        })
        .catch((err) => {
            console.log(err);
        })
})

router.get('/animales/:nombre', (req, res) => {
    const nombre = req.params.nombre
    Animales.find({ nombre })
        .then((doc) => {
            console.log(doc);
            res.json(doc)
        })
        .catch((err) => {
            console.log(err);
        })
})


router.post('/animales', (req, res) => {
    //req.body muestra lso parámetros que le mandas
    console.log(req.body)
    const animalNuevo = {
        nombre: req.body.nombre,
        numeroPatas: req.body.numeroPatas,
        extinguido: req.body.extinguido,
    }
    return new Animales(animalNuevo)
        .save()
        .then((doc) => {
            console.log('Animal grabado en la base de datos', doc)
            res.json(doc)
        })
        .catch((error) => {
            const message = 'Error al guardar animal en la DB'
            console.error(message, error)
            res.status(500).json({ message })
        })
})

router.put('/animales/:id/:sustituto', (req, res) => {
    const id = req.params.id
    Animales.findByIdAndUpdate(id, { extinguido: req.params.sustituto })
        .then((doc) => {
            console.log('Animal con el ID ' + id + ' actualizado ')
            res.send('Animal con el ID ' + id + ' actualizado ')
        })
        .catch((error) => {
            console.log(error)
        })
})

router.delete('/animales/:nombre', (req, res) => {
    const borrar = req.params.nombre
    Animales.findOneAndRemove(borrar)
        .then((doc) => {
            console.log('Animal ' + borrar + ' Eliminado')
            res.send('Animal ' + borrar + ' Eliminado')
        })
})

module.exports = router
